# Telkomsel-image-gallery

![image info](Images-md/app-screenshot.png)

## Getting started
Step By Step to run this application:
1. clone using terminal this repository. you can use http version https://gitlab.com/kufhed/telkomsel-image-gallery.git or ssh version.

![image info](Images-md/repo-url.png)

2. After successfull clone. move to repository dicrectory using `cd telkomsel-image-gallery/` 

![image info](Images-md/repo-clone.png)

3. Run `pod install` from terminal inside repository dicrectory

![image info](Images-md/pod-install.png)

4. After pod install success. Open workspace using this command in terminal `open Telkomsel-Image-Gallery.xcworkspace/`

![image info](Images-md/open-project.png)

5. Run Application by click run icon in XCode

![image info](Images-md/run-xcode.png)

