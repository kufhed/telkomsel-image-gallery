//
//  ArtWorkPaginationModel.swift
//  Telkomsel-Image-Gallery
//
//  Created by Kukuh Frehadtomo on 17/09/23.
//

import Foundation
import SwiftyJSON

class ArtWotkPaginationModel {
    var total: Int = 0
    var limit: Int = 0
    var offset: Int = 0
    var totalPages: Int = 0
    var currentPage: Int = 1
    var nextUrl: String = ""
    
    convenience init(jsonData: AnyObject) {
        self.init()
        let json = JSON(jsonData)
        
        if let total = json["total"].int {
            self.total = total
        }
        if let limit = json["limit"].int {
            self.limit = limit
        }
        if let offset = json["offset"].int {
            self.offset = offset
        }
        if let totalPages = json["total_pages"].int {
            self.totalPages = totalPages
        }
        if let currentPage = json["current_page"].int {
            self.currentPage = currentPage
        }
        if let nextUrl = json["next_url"].string {
            self.nextUrl = nextUrl
        }
    }
}

