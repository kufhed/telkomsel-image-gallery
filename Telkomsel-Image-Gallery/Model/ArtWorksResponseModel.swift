//
//  ArtWorksResponseModel.swift
//  Telkomsel-Image-Gallery
//
//  Created by Kukuh Frehadtomo on 17/09/23.
//

import UIKit
import SwiftyJSON

class ArtWorksResponseModel {
    var pagination: ArtWotkPaginationModel?
    var artworks: [ArtWorkModel] = []
    
    convenience init(jsonData: AnyObject) {
        self.init()
        let json = JSON(jsonData)
        
        self.pagination = ArtWotkPaginationModel(jsonData: json["pagination"].object as AnyObject)
        
        artworks = []
        for artwork in json["data"].arrayValue {
            artworks.append(ArtWorkModel(jsonData: artwork as AnyObject))
        }
    }
}
