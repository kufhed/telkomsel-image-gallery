//
//  ArtWorkModel.swift
//  Telkomsel-Image-Gallery
//
//  Created by Kukuh Frehadtomo on 17/09/23.
//

import Foundation
import SwiftyJSON

class ArtWorkModel {
    var id: Int = 0
    var apiModel: String = ""
    var apiLink: String = ""
    var isBoosted: Bool = false
    var title: String = ""
    var mainReferenceNumber: String = ""
    var hasNotBeenViewedMuch: Bool = false
    var dateStart: Int = 0
    var dateEnd: Int = 0
    var dateDisplay: String = ""
    var dateQualifierTitle: String = ""
    var artistDisplay: String = ""
    var placeOfOrigin: String = ""
    var description: String = ""
    var dimensions: String = ""
    var mediumDisplay: String = ""
    var inscriptions: String = ""
    var creditLine: String = ""
    var publicationHistory: String = ""
    var exhibitionHistory: String = ""
    var edition: String = ""
    var publishingVerificationLevel: String = ""
    var internalDepartmentId: Int = 0
    var fiscalYear: Int = 0
    var isPublicDomain: Bool = false
    var isZoomable: Bool = false
    var maxZoomWindowSize: Int = 0
    var hasMultimediaResources: Bool = false
    var hasEducationalResources: Bool = false
    var hasAdvancedImaging: Bool = false
    var colorfulness: Double = 0.0
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    var latlon: String = ""
    var isOnView: Bool = false
    var onLoanDisplay: String = ""
    var galleryTitle: String = ""
    var galleryId: Int = 0
    var nomismaId: Int = 0
    var artworkTypeTitle: String = ""
    var artworkTypeId: Int = 0
    var departmentTitle: String = ""
    var departmentId: String = ""
    var artistId: Int = 0
    var artistTitle: String = ""
    var imageId: String = ""
    var sourceUpdatedAt: String = ""
    var updatedAT: String = ""
    var timestamp: String = ""
    
    convenience init(jsonData: AnyObject) {
        self.init()
        let json = JSON(jsonData)
        
        if let id = json["id"].int {
            self.id = id
        }
        if let apiModel = json["api_model"].string {
            self.apiModel = apiModel
        }
        if let apiLink = json["api_link"].string {
            self.apiLink = apiLink
        }
        if let isBoosted = json["is_boosted"].bool {
            self.isBoosted = isBoosted
        }
        if let title = json["title"].string {
            self.title = title
        }
        if let mainReferenceNumber = json["main_reference_number"].string {
            self.mainReferenceNumber = mainReferenceNumber
        }
        if let hasNotBeenViewedMuch = json["has_not_been_viewed_much"].bool {
            self.hasNotBeenViewedMuch = hasNotBeenViewedMuch
        }
        if let dateStart = json["date_start"].int {
            self.dateStart = dateStart
        }
        if let dateEnd = json["date_end"].int {
            self.dateEnd = dateEnd
        }
        if let dateDisplay = json["date_display"].string {
            self.dateDisplay = dateDisplay
        }
        if let dateQualifierTitle = json["date_qualifier_title"].string {
            self.dateQualifierTitle = dateQualifierTitle
        }
        if let artistDisplay = json["artist_display"].string {
            self.artistDisplay = artistDisplay
        }
        if let placeOfOrigin = json["place_of_origin"].string {
            self.placeOfOrigin = placeOfOrigin
        }
        if let description = json["description"].string {
            self.description = description
        }
        if let dimensions = json["dimensions"].string {
            self.dimensions = dimensions
        }
        if let mediumDisplay = json["medium_display"].string {
            self.mediumDisplay = mediumDisplay
        }
        if let inscriptions = json["inscriptions"].string {
            self.inscriptions = inscriptions
        }
        if let creditLine = json["credit_line"].string {
            self.creditLine = creditLine
        }
        if let publicationHistory = json["publication_history"].string {
            self.publicationHistory = publicationHistory
        }
        if let exhibitionHistory = json["exhibition_history"].string {
            self.exhibitionHistory = exhibitionHistory
        }
        if let edition = json["edition"].string {
            self.edition = edition
        }
        if let publishingVerificationLevel = json["publishing_verification_level"].string {
            self.publishingVerificationLevel = publishingVerificationLevel
        }
        if let internalDepartmentId = json["internal_department_id"].int {
            self.internalDepartmentId = internalDepartmentId
        }
        if let fiscalYear = json["fiscal_year"].int {
            self.fiscalYear = fiscalYear
        }
        if let isPublicDomain = json["is_public_domain"].bool {
            self.isPublicDomain = isPublicDomain
        }
        if let isZoomable = json["is_zoomable"].bool {
            self.isZoomable = isZoomable
        }
        if let maxZoomWindowSize = json["max_zoom_window_size"].int {
            self.maxZoomWindowSize = maxZoomWindowSize
        }
        if let hasMultimediaResources = json["has_multimedia_resources"].bool {
            self.hasMultimediaResources = hasMultimediaResources
        }
        if let hasEducationalResources = json["has_educational_resources"].bool {
            self.hasEducationalResources = hasEducationalResources
        }
        if let hasAdvancedImaging = json["has_advanced_imaging"].bool {
            self.hasAdvancedImaging = hasAdvancedImaging
        }
        if let colorfulness = json["colorfulness"].double {
            self.colorfulness = colorfulness
        }
        if let latitude = json["latitude"].double {
            self.latitude = latitude
        }
        if let longitude = json["longitude"].double {
            self.longitude = longitude
        }
        if let latlon = json["latlon"].string {
            self.latlon = latlon
        }
        if let isOnView = json["is_on_view"].bool {
            self.isOnView = isOnView
        }
        if let onLoanDisplay = json["on_loan_display"].string {
            self.onLoanDisplay = onLoanDisplay
        }
        if let galleryTitle = json["gallery_title"].string {
            self.galleryTitle = galleryTitle
        }
        if let galleryId = json["gallery_id"].int {
            self.galleryId = galleryId
        }
        if let nomismaId = json["nomisma_id"].int {
            self.nomismaId = nomismaId
        }
        if let artworkTypeTitle = json["artwork_type_title"].string {
            self.artworkTypeTitle = artworkTypeTitle
        }
        if let artworkTypeId = json["artwork_type_id"].int {
            self.artworkTypeId = artworkTypeId
        }
        if let departmentTitle = json["department_title"].string {
            self.departmentTitle = departmentTitle
        }
        if let departmentId = json["department_id"].string {
            self.departmentId = departmentId
        }
        if let artistId = json["artist_id"].int {
            self.artistId = artistId
        }
        if let artistTitle = json["artist_title"].string {
            self.artistTitle = artistTitle
        }
        if let imageId = json["image_id"].string {
            self.imageId = imageId
        }
        if let sourceUpdatedAt = json["source_updated_at"].string {
            self.sourceUpdatedAt = sourceUpdatedAt
        }
        if let updatedAT = json["updated_at"].string {
            self.updatedAT = updatedAT
        }
        if let timestamp = json["timestamp"].string {
            self.timestamp = timestamp
        }
    }
}
