//
//  ErrorModel.swift
//  Telkomsel-Image-Gallery
//
//  Created by Kukuh Frehadtomo on 17/09/23.
//

import Foundation

enum EnumErrorId: String {
    case serviceCallCancelled = "-99"
}

struct ErrorModel: Error {
    var errorMessage: String
    var errorId: String
}
