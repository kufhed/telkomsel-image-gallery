//
//  AppDelegate.swift
//  Telkomsel-Image-Gallery
//
//  Created by Kukuh Frehadtomo on 17/09/23.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setupInitialScreen()
        return true
    }
    
    private func setupInitialScreen() {
        self.window?.tintColor = UIColor.white
        let initialVC: UINavigationController = UINavigationController(rootViewController: HomeArtWorkGalleryViewController())
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = initialVC
        self.window?.makeKeyAndVisible()
    }
}

