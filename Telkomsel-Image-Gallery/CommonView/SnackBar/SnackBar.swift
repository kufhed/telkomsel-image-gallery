//
//  SnackBar.swift
//  Telkomsel-Image-Gallery
//
//  Created by Kukuh Frehadtomo on 17/09/23.
//
import UIKit

class SnackbarView: UIView {
    var messageLabel: UILabel!

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.red.withAlphaComponent(0.8)
        layer.cornerRadius = 10
        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        messageLabel = UILabel()
        messageLabel.textColor = UIColor.white
        messageLabel.textAlignment = .center
        addSubview(messageLabel)
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        messageLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        messageLabel.textAlignment = .left
    }
}
