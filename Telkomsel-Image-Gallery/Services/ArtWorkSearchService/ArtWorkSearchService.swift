//
//  ArtWorkSearchService.swift
//  Telkomsel-Image-Gallery
//
//  Created by Kukuh Frehadtomo on 17/09/23.
//

import Foundation
import Alamofire

class ArtWorkSearchService: BaseService {
    typealias Response = ArtWorksResponseModel
    var query: String = ""
    var nextPageUrl: String = ""
    
    func getURL() -> String {
        if nextPageUrl.isEmpty {
            return BaseEndPoint.LIST_ARTWORK_URL+"search=\(query)"
        }
        return nextPageUrl
    }
    
    func getMethod() -> Alamofire.HTTPMethod {
        return .get
    }
    
    func getParameters() -> [String : AnyObject] {
        return [:]
    }
    
    func result(resultJSON: AnyObject) -> Response {
        let artWorkResponse = ArtWorksResponseModel(jsonData: resultJSON)
        return artWorkResponse
    }
}
