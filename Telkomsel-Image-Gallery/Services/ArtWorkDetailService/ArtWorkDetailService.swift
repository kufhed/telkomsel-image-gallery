//
//  ArtWorkDetailService.swift
//  Telkomsel-Image-Gallery
//
//  Created by Kukuh Frehadtomo on 17/09/23.
//
import Foundation
import Alamofire

class ArtWorkDetailService: BaseService {
    typealias Response = ArtWorkModel
    var url: String = ""
    
    func getURL() -> String {
        return url
    }
    
    func getMethod() -> Alamofire.HTTPMethod {
        return .get
    }
    
    func getParameters() -> [String : AnyObject] {
        return [:]
    }
    
    func result(resultJSON: AnyObject) -> Response {
        let artWorkResponse = ArtWorkModel(jsonData: resultJSON)
        return artWorkResponse
    }
}

