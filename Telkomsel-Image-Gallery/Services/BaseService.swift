//
//  BaseService.swift
//  Telkomsel-Image-Gallery
//
//  Created by Kukuh Frehadtomo on 17/09/23.
//

import UIKit
import RxSwift
import Alamofire

enum ServiceError: Error {
    case invalidURL
}

protocol BaseService {
    associatedtype Response
    func getURL() -> String
    func getMethod() -> Alamofire.HTTPMethod
    func getParameters() -> [String: AnyObject]
    func result(resultJSON: AnyObject) -> Response
}

extension BaseService {
    func startRequest() -> Observable<Response> {
        return Observable.create { (observer) -> Disposable in
            _ = AF.request(getURL(), method: getMethod(), parameters: getParameters(), encoding: URLEncoding.default)
                .validate()
                .responseJSON(completionHandler: {
                    (response) in
                    debugPrint(response)
                    switch response.result {
                    case .success(let responseValue):
                        observer.onNext(self.result(resultJSON: responseValue as AnyObject))
                    case .failure(let responseError):
                        var errorResult = ErrorModel(errorMessage: "", errorId: "")
                        switch responseError {
                        case .sessionTaskFailed(error: let urlError as URLError):
                            switch urlError.code {
                            case .cancelled:
                                errorResult.errorId = EnumErrorId.serviceCallCancelled.rawValue
                            default:
                                break
                            }
                        default:
                            errorResult.errorMessage = Constant.ERROR_SERVICE_GENERAL
                        }
                        observer.onError(errorResult)
                    }
                    observer.onCompleted()
                })
            
            return Disposables.create {}
        }
    }
        
    func encodeURL()  -> URLConvertible {
        var allowedQueryParamAndKey = NSCharacterSet.urlQueryAllowed
        allowedQueryParamAndKey.remove(charactersIn: ";/?:@&=+$, ")
        guard let encodedURL = getURL().addingPercentEncoding(withAllowedCharacters: allowedQueryParamAndKey) else { return ""}
        return encodedURL
    }
}
