//
//  ArtWorkListService.swift
//  Telkomsel-Image-Gallery
//
//  Created by Kukuh Frehadtomo on 17/09/23.
//

import Foundation
import Alamofire
import RxSwift

class ArtWorkListService: BaseService {
    typealias Response = ArtWorksResponseModel
    var url: String = ""
    var page: String = ""
    var limit: String = ""
    
    func getURL() -> String {
        if url.isEmpty {
            return BaseEndPoint.LIST_ARTWORK_URL+url+"page=\(page)&limit=\(limit)"
        }
        return url
    }
    
    func getMethod() -> Alamofire.HTTPMethod {
        return .get
    }
    
    func getParameters() -> [String : AnyObject] {
        return [:]
    }
    
    func result(resultJSON: AnyObject) -> Response {
        let artWorkResponse = ArtWorksResponseModel(jsonData: resultJSON)
        return artWorkResponse
    }
}
