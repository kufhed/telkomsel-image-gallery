//
//  Constant.swift
//  Telkomsel-Image-Gallery
//
//  Created by Kukuh Frehadtomo on 17/09/23.
//

import Foundation

class Constant {
    static let ERROR_CONVERT_URL = "Failed connect to service."
    static let ERROR_SERVICE_GENERAL = "Sorry,something wrong. Please try again."
}
