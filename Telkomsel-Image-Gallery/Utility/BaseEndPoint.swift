//
//  BaseEndPoint.swift
//  Telkomsel-Image-Gallery
//
//  Created by Kukuh Frehadtomo on 17/09/23.
//

import UIKit

class BaseEndPoint {
    static let BASE_URL = "https://api.artic.edu/"
    static let LIST_ARTWORK_URL = BASE_URL+"api/v1/artworks?"
    static let SEARCH_ARTWORK_URL = BASE_URL+"api/v1/artworks/search?"
}
