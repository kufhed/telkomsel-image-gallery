//
//  UIView+Extension.swift
//  Telkomsel-Image-Gallery
//
//  Created by Kukuh Frehadtomo on 17/09/23.
//

import UIKit

extension UIView {
    func setupRoundAndShadow() {
        self.setupRound()
        self.setupShadow()
    }
    
    func setupRound(radius: CGFloat = 8.0) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
    
    func setupShadow(shadowColor: UIColor = UIColor.gray, radius: CGFloat = 8.0) {
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowRadius = radius
    }
}
