//
//  HomeArtWorkGalleryViewController.swift
//  Telkomsel-Image-Gallery
//
//  Created by Kukuh Frehadtomo on 17/09/23.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class HomeArtWorkGalleryViewController: UIViewController {
    private let viewModel = HomeArtWorkGalleryViewModel()
    private let disposeBag = DisposeBag()
  
    @IBOutlet var galleryCollectionView: UICollectionView!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var parentGalleryContainer: UIView!
    
    private var activityIndicator = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        requestGalleryImage()
    }
    
    private func setupView() {
        setupCollectionView()
        bindEvent()
        bindData()
    }
    
    private func setNavigationTitle() {
        self.navigationItem.title = "Home Screen"
    }
    
    private func requestGalleryImage(isLoadMore: Bool = false) {
        viewModel.getArtWorksData(isLoadMore: isLoadMore).subscribe(onNext: {
            (artWorksData) in
        }).disposed(by: disposeBag)
    }
    
    private func requestSearchGalleryImage(isLoadMore: Bool = false) {
        viewModel.searchArtWork(query: viewModel.lastQuery, isLoadMore: isLoadMore).subscribe(onNext: {
            (artWorsData) in
        }).disposed(by: disposeBag)
    }
    
    private func setupCollectionView() {
        self.galleryCollectionView.register(UINib(nibName: HomeArtWorkGalleryViewModel.CollectionViewCellType.galleryCell.rawValue, bundle: nil), forCellWithReuseIdentifier: HomeArtWorkGalleryViewModel.CollectionViewCellType.galleryCell.rawValue)
        self.galleryCollectionView.register(UINib(nibName: HomeArtWorkGalleryViewModel.CollectionViewCellType.loadMoreFooter.rawValue, bundle: .main), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: HomeArtWorkGalleryViewModel.CollectionViewCellType.loadMoreFooter.rawValue)
        self.galleryCollectionView.delegate = self
        self.galleryCollectionView.dataSource = self
        self.galleryCollectionView.reloadData()
    }
    
    private func bindEvent() {
        viewModel.isLoadingMoreData.asObservable().subscribe(onNext: {
            [weak self] (isLoading) in
            self?.galleryCollectionView.collectionViewLayout.invalidateLayout()
        }).disposed(by: disposeBag)
        
        searchBar.rx.text
            .orEmpty
            .skip(1)
            .debounce(RxTimeInterval.seconds(1), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .subscribe(onNext: {
                [weak self] query in
                if query.isEmpty {
                    self?.requestGalleryImage()
                }
                self?.viewModel.searchQuery.onNext(query)
            })
            .disposed(by: disposeBag)
        
        viewModel.showLoading.subscribe(onNext: {
            [weak self] isShow in
            guard let self = self else { return }
            self.showCenterLoading()
        }).disposed(by: disposeBag)
        
        viewModel.hideLoading.subscribe(onNext: {
            [weak self] isShow in
            guard let self = self else { return }
            self.hideCenterLoading()
        }).disposed(by: disposeBag)
        
        viewModel.showErrorLoadArtWorks.subscribe(onNext: {
            [weak self] (error) in
            guard let self = self else { return }
            self.showSnackbar(withMessage: error.errorMessage)
        }).disposed(by: disposeBag)
        
        let tapGesture = UITapGestureRecognizer()
        tapGesture.rx.event.subscribe(onNext: { [weak self] _ in
            self?.view.endEditing(true)
        }).disposed(by: disposeBag)
        view.addGestureRecognizer(tapGesture)
    }
    
    private func bindData() {
        viewModel.artWorks.subscribe(onNext: {
            [weak self] artswork in
            guard let self = self else { return }
            self.galleryCollectionView.reloadData()
        }).disposed(by: disposeBag)
    }
    
    private func showCenterLoading() {
        galleryCollectionView.isHidden = true
        activityIndicator = UIActivityIndicatorView()
        activityIndicator.startAnimating()
        parentGalleryContainer.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.centerYAnchor.constraint(equalTo: parentGalleryContainer.centerYAnchor).isActive = true
        activityIndicator.centerXAnchor.constraint(equalTo: parentGalleryContainer.centerXAnchor).isActive = true
    }
    
    private func hideCenterLoading() {
        galleryCollectionView.isHidden = false
        activityIndicator.removeFromSuperview()
    }
    
    private func showSnackbar(withMessage message: String) {
        let snackbar = SnackbarView(frame: CGRect(x: 0, y: view.frame.size.height, width: view.frame.size.width, height: 90.0))
        snackbar.messageLabel.text = message
        view.addSubview(snackbar)
        
        UIView.animate(withDuration: 0.3, animations: {
            snackbar.frame.origin.y -= snackbar.frame.size.height
        }, completion: { _ in
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                [weak self] in
                self?.hideSnackbar(snackbar)
            }
        })
    }
    
    func hideSnackbar(_ snackbar: SnackbarView) {
        UIView.animate(withDuration: 0.3, animations: {
            snackbar.frame.origin.y += snackbar.frame.size.height
        }, completion: { _ in
            snackbar.removeFromSuperview()
        })
    }
}

extension HomeArtWorkGalleryViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UISearchBarDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.artWorks.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeArtWorkGalleryViewModel.CollectionViewCellType.galleryCell.rawValue, for: indexPath) as? GalleryItemCell else  {
            return UICollectionViewCell()
        }
        
        if viewModel.artWorks.value.indices.contains(indexPath.row) {
            let item = viewModel.artWorks.value[indexPath.row]
            cell.setupData(artWork: item)
            return cell
        }
        
        return UICollectionViewCell()
        
    }
    

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = view.bounds.width/3
        return CGSize(width: width, height: 120.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if viewModel.isLoadingMoreData.value {
            return CGSize(width: view.bounds.width, height: 50.0)
        }
        return CGSize(width: 0.0, height: 0.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionFooter {
            let view = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: HomeArtWorkGalleryViewModel.CollectionViewCellType.loadMoreFooter.rawValue, for: indexPath)
             return view
        } else {
            return UICollectionReusableView()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == collectionView.numberOfItems(inSection: indexPath.section) - 1 {
            if viewModel.canLoadMoreData(page: viewModel.pagination.currentPage+1) {
                if viewModel.lastQuery.isEmpty {
                    requestGalleryImage(isLoadMore: true)
                } else {
                    requestSearchGalleryImage(isLoadMore: true)
                }
            }
        }
    }
}
