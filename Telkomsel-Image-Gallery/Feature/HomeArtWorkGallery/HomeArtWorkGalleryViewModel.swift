//
//  HomeArtWorkGalleryViewModel.swift
//  Telkomsel-Image-Gallery
//
//  Created by Kukuh Frehadtomo on 17/09/23.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class HomeArtWorkGalleryViewModel {
    
    enum CollectionViewCellType: String {
        case galleryCell = "GalleryItemCell"
        case loadMoreFooter = "LoadMoreFooterReusableView"
    }
    
    let artWorkListService: ArtWorkListService = ArtWorkListService()
    let artWorkSearchService: ArtWorkSearchService = ArtWorkSearchService()
    let artWorkDetailService: ArtWorkDetailService = ArtWorkDetailService()
    let disposeBag: DisposeBag = DisposeBag()
    let limitDataPerPage: Int = 15
    
    var artWorks: BehaviorRelay<[ArtWorkModel]> = BehaviorRelay(value: [])
    var pagination: ArtWotkPaginationModel = ArtWotkPaginationModel()
    var showErrorLoadArtWorks: PublishSubject<ErrorModel> = PublishSubject<ErrorModel>()
    var isFirstLoadingPage: Bool = true
    var isLoadingMoreData: BehaviorRelay<Bool> = BehaviorRelay(value: false)
    var searchQuery: PublishSubject<String> = PublishSubject()
    var lastQuery: String = ""
    var showLoading: PublishSubject<Bool> = PublishSubject<Bool>()
    var hideLoading: PublishSubject<Bool> = PublishSubject<Bool>()
    
    init() {
        self.bindEvent()
    }
    
    private func bindEvent() {
        searchQuery.subscribe(onNext: {
            [weak self] (query) in
            guard let self = self else { return }
            self.lastQuery = query
            if !self.lastQuery.isEmpty {
                _ = self.searchArtWork(query: self.lastQuery).subscribe(onNext: {
                    dataArtWork in
                    print(dataArtWork)
                }).disposed(by: disposeBag)
            } else {
                self.resetDataGallery()
            }
        }).disposed(by: disposeBag)
    }
    
    func getArtWorksData(isLoadMore: Bool = false) -> Observable<ArtWorksResponseModel> {
        if isLoadMore {
            artWorkListService.page = "\(pagination.currentPage+1)"
            artWorkListService.url = pagination.nextUrl
            isLoadingMoreData.accept(true)
        } else {
            pagination = ArtWotkPaginationModel()
            artWorkListService.page = "\(pagination.currentPage)"
            artWorkListService.limit = "\(limitDataPerPage)"
            showLoading.onNext(true)
        }
        return artWorkListService.startRequest().do(onNext: {
            [weak self] (artWorksResponse) in
            guard let self = self else { return }
            
            if isNewResponseDataEmpty(artWorksResponse: artWorksResponse) {
                handleDataIsEmpty()
                return
            }
            
            if !isLoadMore {
                isFirstLoadingPage = false
                hideLoading.onNext(true)
            } else {
                isLoadingMoreData.accept(false)
            }
            
            if pagination.currentPage == 1 {
                self.resetDataGallery()
                self.artWorks.accept(artWorksResponse.artworks)
            } else {
                var currentArtWorks = self.artWorks.value
                currentArtWorks.append(contentsOf: artWorksResponse.artworks)
                self.artWorks.accept(currentArtWorks)
            }
            
            self.setPagination(artWorksResponse: artWorksResponse)
        }, onError: {
            [weak self] (error) in
            guard let self = self else { return }
            if let errorModel = error as? ErrorModel {
                self.showErrorLoadArtWorks.onNext(errorModel)
            }
        })
    }
    
    func searchArtWork(query: String, isLoadMore: Bool = false) -> Observable<ArtWorksResponseModel> {
        if !isLoadMore {
            pagination = ArtWotkPaginationModel()
            showLoading.onNext(true)
        } else {
            artWorkListService.url = pagination.nextUrl
            isLoadingMoreData.accept(true)
        }
        
        artWorkSearchService.query = query
        return artWorkSearchService.startRequest().do(onNext: {
            [weak self] (artWorksResponse) in
            guard let self = self else { return }
            
            if isNewResponseDataEmpty(artWorksResponse: artWorksResponse) {
                handleDataIsEmpty()
                return
            }
            
            if !isLoadMore {
                self.resetDataGallery()
                hideLoading.onNext(true)
                self.artWorks.accept(artWorksResponse.artworks)
            } else {
                isLoadingMoreData.accept(false)
                var artWorksData = self.artWorks.value
                artWorksData.append(contentsOf: artWorksResponse.artworks)
                artWorks.accept(artWorksData)
            }
            
            self.setPagination(artWorksResponse: artWorksResponse)
        }, onError: {
            [weak self] (error) in
            guard let self = self else { return }
            if let errorModel = error as? ErrorModel {
                self.showErrorLoadArtWorks.onNext(errorModel)
            }
        })
    }
    
    func canLoadMoreData(page: Int) -> Bool {
        if isReachMaxPage(page: page) || isLoadingMoreData.value {
            return false
        }
        return true
    }

    func isReachMaxPage(page: Int) -> Bool {
        if page > 1 && page <= pagination.totalPages {
            return false
        }
        return true
    }
    
    private func resetDataGallery() {
        artWorks.accept([])
    }
    
    private func setPagination(artWorksResponse: ArtWorksResponseModel) {
        if let paginationPage = artWorksResponse.pagination {
            self.pagination = paginationPage
        }
    }
    
    private func isNewResponseDataEmpty(artWorksResponse: ArtWorksResponseModel) -> Bool{
        if artWorksResponse.artworks.isEmpty {
            return true
        }
        return false
    }
    
    private func handleDataIsEmpty() {
        hideLoading.onNext(true)
        isLoadingMoreData.accept(false)
        let errorModel = ErrorModel(errorMessage: "Sorry, Data is Empty. please try again.", errorId: "")
        self.showErrorLoadArtWorks.onNext(errorModel)
    }
}
