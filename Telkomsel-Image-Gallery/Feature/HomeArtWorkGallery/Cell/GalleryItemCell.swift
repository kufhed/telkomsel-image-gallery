//
//  GalleryItemCell.swift
//  Telkomsel-Image-Gallery
//
//  Created by Kukuh Frehadtomo on 17/09/23.
//

import UIKit
import SDWebImage

class GalleryItemCell: UICollectionViewCell {

    @IBOutlet var galleryImage: UIImageView!
    @IBOutlet var imageDescription: UILabel!
    @IBOutlet var containerImage: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    func setupData(artWork: ArtWorkModel) {
        galleryImage.sd_setImage(with: URL(string: "https://www.artic.edu/iiif/2/\(artWork.imageId)/full/843,/0/default.jpg"))
        imageDescription.text = artWork.title
    }
    
    private func setupView() {
        containerImage.setupRoundAndShadow()
    }
}
